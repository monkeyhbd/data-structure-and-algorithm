#ifndef QUEUE_H
#define QUEUE_H


typedef int ElemType;
struct queue;
struct queueNode;
typedef struct queue Queue;
typedef struct queueNode QueueNode;


int QueueInit(Queue* que);
int QueueDestroy(Queue* que);


int QueueDequeue(Queue* que);
int QueueEnqueue(Queue* que, ElemType elem);
ElemType QueueFront(Queue* que);
int QueuePrint(Queue* que);


#endif
