#include <stdio.h>
#include <stdlib.h>
#include "Queue.h"


struct queue {
	QueueNode*   head;
	QueueNode*   tail;
};


struct queueNode {
	ElemType     data;
	QueueNode*   next;
};


int QueueInit(Queue* que) {
	que->head = (QueueNode*) malloc(sizeof(QueueNode));
	if (que->head == NULL) {
		return 1;
	}
	que->head->next = NULL;
	que->tail = que->head;
	return 0;
}


int QueueEnqueue(Queue* que, ElemType elem) {
	QueueNode* newNode = (QueueNode*) malloc(sizeof(QueueNode));
	if (newNode == NULL) {
		return 1;
	}
	newNode->data = elem;
	newNode->next = NULL;
	que->tail->next = newNode;
	que->tail = newNode;
	return 0;
}


int QueueDequeue(Queue* que) {
	QueueNode* node = que->head->next;
	que->head->next = node->next;
	free(node);
	return 0;
}


int QueueIsEmpty(Queue* que) {
	return que->head == que->tail;
}


ElemType QueueFront(Queue* que) {
	return que->head->next->data;
}


int QueuePrint(Queue* que) {
	for (QueueNode* node = que->head->next; node != NULL; node = node->next) {
		printf("%d ", node->data);
	}
	printf("\n");
	return 0;
}


int QueueDestroy(Queue* que) {
	QueueNode* current = que->head;
	QueueNode* next;
	while (current != NULL) {
		next = current->next;
		free(current);
		current = next;
	}
	que->head = NULL;
	return 0;
}


int main() {
	Queue que;
	QueueInit(&que);
	for (int x = 1; x <= 8; x += 1) {
		QueueEnqueue(&que, x);
	}
	QueuePrint(&que);
	for (int x = 0; x < 5; x += 1) {
		QueueDequeue(&que);
	}
	QueuePrint(&que);
	for (int x = 0; x < 3; x += 1) {
		QueueDequeue(&que);
	}
	QueuePrint(&que);
	QueueDestroy(&que);
	return 0;
}
