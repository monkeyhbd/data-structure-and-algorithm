#ifndef STACK_H
#define STACK_H


#define STACK_INIT_SIZE   100
#define STACK_INCREMENT   10


typedef int ElemType;


struct stack {
	ElemType*   heap;
	int         top;
	int         size;
};


typedef struct stack Stack;


int StackInit(Stack* stack);
int StackDestroy(Stack* stack);


int StackIsEmpty(Stack* stack);
int StackPop(Stack* stack);
int StackPrint(Stack* stack);
int StackPush(Stack* stack, ElemType elem);
ElemType StackTop(Stack* stack);


#endif
