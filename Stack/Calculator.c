#include <stdio.h>
#include "Stack.h"


ElemType calculate(char notation[]) {
	Stack stk;
	StackInit(&stk);
	int idx = 0;
	while (notation[idx] != '\0') {
		if ('0' <= notation[idx] && notation[idx] <= '9') {
			ElemType tmp;
			sscanf(&notation[idx], "%d", &tmp);
			StackPush(&stk, tmp);
			for (; notation[idx] != ' ' && notation[idx] != '\0'; idx += 1) {}
		}
		else if (notation[idx] == '+') {
			ElemType b = StackTop(&stk);
			StackPop(&stk);
			ElemType a = StackTop(&stk);
			StackPop(&stk);
			StackPush(&stk, a + b);
			for (; notation[idx] != ' ' && notation[idx] != '\0'; idx += 1) {}
		}
		else if (notation[idx] == '-') {
			ElemType b = StackTop(&stk);
			StackPop(&stk);
			ElemType a = StackTop(&stk);
			StackPop(&stk);
			StackPush(&stk, a - b);
			for (; notation[idx] != ' ' && notation[idx] != '\0'; idx += 1) {}
		}
		else if (notation[idx] == '*') {
			ElemType b = StackTop(&stk);
			StackPop(&stk);
			ElemType a = StackTop(&stk);
			StackPop(&stk);
			StackPush(&stk, 1.0 * a * b);
			for (; notation[idx] != ' ' && notation[idx] != '\0'; idx += 1) {}
		}
		else if (notation[idx] == '/') {
			ElemType b = StackTop(&stk);
			StackPop(&stk);
			ElemType a = StackTop(&stk);
			StackPop(&stk);
			StackPush(&stk, 1.0 * a / b);
			for (; notation[idx] != ' ' && notation[idx] != '\0'; idx += 1) {}
		}
		else {
			idx += 1;
		}
		StackPrint(&stk);
	}
	return StackTop(&stk);
}


int main() {
	char notation[1000];
	gets(notation);
	printf("%s = %d\n", notation, calculate(notation));
	return 0;
}

