#ifndef ARRAY_H
#define ARRAY_H


#define ARRAY_INIT_SIZE   100
#define ARRAY_INCREMENT   10


typedef int ElemType;
struct array;
typedef struct array Array;


int ArrayInit(Array* arr);
int ArrayDestroy(Array* arr);


int ArrayAppend(Array* arr, ElemType elem);
int ArrayClear(Array* arr);
int ArrayGrow(Array* arr);
int ArrayIndex(Array* arr, ElemType elem);
int ArrayInsert(Array* arr, int idx, ElemType elem);
int ArrayIsEmpty(Array* arr);
int ArrayLength(Array* arr);
ElemType ArrayPop(Array* arr, int idx);
int ArrayPrint(Array* arr);


#endif
