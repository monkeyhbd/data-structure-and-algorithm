#include "Sort.h"


void SelectionSort(ElemType arr[], int len) {
	for (int x = 0; x < len; x += 1) {
		int minIdx = x;
		for (int idx = x; idx < len; idx += 1) {
			if (arr[idx] < arr[minIdx]) {
				minIdx = idx;
			}
		}
		ElemType tmp = arr[x];
		arr[x] = arr[minIdx];
		arr[minIdx] = tmp;
	}
}
