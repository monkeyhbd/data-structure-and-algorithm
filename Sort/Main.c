#include <stdio.h>
#include "Sort.h"


void PrintArray(ElemType arr[], int len) {
	for (int idx = 0; idx < len; idx += 1) {
		printf("%d ", arr[idx]);
	}
	printf("\n");
}


int main() {
	ElemType arr1[] = {9, 8, 7, 6, 5, 4, 3, 2, 1};
	ElemType arr2[] = {1};
	ElemType arr3[] = {};

	PrintArray(arr1, sizeof(arr1) / sizeof(ElemType));
	MergeSort(arr1, sizeof(arr1) / sizeof(ElemType));
	PrintArray(arr1, sizeof(arr1) / sizeof(ElemType));

	PrintArray(arr2, sizeof(arr2) / sizeof(ElemType));
	MergeSort(arr2, sizeof(arr2) / sizeof(ElemType));
	PrintArray(arr2, sizeof(arr2) / sizeof(ElemType));

	PrintArray(arr3, sizeof(arr3) / sizeof(ElemType));
	MergeSort(arr3, sizeof(arr3) / sizeof(ElemType));
	PrintArray(arr3, sizeof(arr3) / sizeof(ElemType));

	return 0;
}
