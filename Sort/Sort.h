typedef int ElemType;


void BubbleSort(ElemType arr[], int len);
void InsertionSort(ElemType arr[], int len);
void SelectionSort(ElemType arr[], int len);
void MergeSort(ElemType arr[], int len);
