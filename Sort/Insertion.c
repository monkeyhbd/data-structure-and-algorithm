#include "Sort.h"


void InsertionSort(ElemType arr[], int len) {
	for (int x = 1; x < len; x += 1) {
		ElemType tmp = arr[x];
		int idx = x - 1;
		for (; idx > -1 && arr[idx] > tmp; idx -= 1) {
			arr[idx+1] = arr[idx];
		}
		arr[idx+1] = tmp;
	}
}
