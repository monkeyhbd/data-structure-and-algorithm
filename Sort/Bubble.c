#include "Sort.h"


void BubbleSort(ElemType arr[], int len) {
	for (int x = 0; x < len; x += 1) {
		for (int idx = 0; idx < len - x - 1; idx += 1) {
			if (arr[idx] > arr[idx+1]) {
				ElemType tmp = arr[idx];
				arr[idx] = arr[idx+1];
				arr[idx+1] = tmp;
			}
		}
	}
}
