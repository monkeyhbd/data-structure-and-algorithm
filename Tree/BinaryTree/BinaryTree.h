#ifndef BINARY_TREE_H
#define BINARY_TREE_H


typedef int ElemType;
struct binaryTree;
struct binaryTreeNode;
typedef struct binaryTree BinaryTree;
typedef struct binaryTreeNode BinaryTreeNode;


int BinaryTreeInit(BinaryTree* tree);
int BinaryTreeDestory(BinaryTree* tree);


BinaryTreeNode* BinaryTreeAppendChild(BinaryTreeNode* parent, ElemType elem, int right);
int BinaryTreePrint(BinaryTreeNode* root);
int BinaryTreePrintBeauty(BinaryTreeNode* root);
int BinaryTreeDeleteNode(BinaryTreeNode* root, BinaryTreeNode* target);
int BinaryTreeDeleteNodeCore(BinaryTreeNode* target);


struct binaryTree {
	BinaryTreeNode*   root;
};


struct binaryTreeNode {
	ElemType          data;
	BinaryTreeNode*   left;
	BinaryTreeNode*   right;
};


#endif
