#ifndef TREE_H
#define TREE_H


typedef int ElemType;
struct tree;
struct treeNode;
typedef struct tree Tree;
typedef struct treeNode TreeNode;


int TreeInit(Tree* tree);
int TreeDestory(Tree* tree);


// Append a child node to parent, to store elem. Return the address of child.
TreeNode* TreeAppendChild(TreeNode* parent, ElemType elem);

// Append a existing node to parent.
int TreeAppendNode(TreeNode* parent, TreeNode* node);

// Clone the tree of a specified root. Return cloned tree's root address.
TreeNode* TreeClone(TreeNode* root);

// Copy all sub-tree from src node to des node.
int TreeCopyAllChild(TreeNode* des, TreeNode* src);

// Delete target sub-tree from the tree of root.
int TreeDeleteNode(TreeNode* root, TreeNode* target);

// Delete target tree, but not modified the pointer of
// parent's firstChild or sibling's nextSibling.
int TreeDeleteNodeCore(TreeNode* target);

// Print all node in the tree of root, with its
// address, data, firstChild address and nextSibling address.
int TreeDetail(TreeNode* root);

// Return the address of child's parent node in the tree of root.
TreeNode* TreeParent(TreeNode* root, TreeNode* child);

// Print tree.
int TreePrint(TreeNode* root);

// Print tree with branch.
int TreePrintBeauty(TreeNode* root);


struct tree {
	TreeNode*   root;
};


struct treeNode {
	ElemType    data;
	TreeNode*   firstChild;
	TreeNode*   nextSibling;
};


#endif
