#ifndef BST_H
#define BST_H


typedef int ElemType;
struct bst;
struct bstNode;
typedef struct bst BST;
typedef struct bstNode BSTNode;


int BSTInit(BST* tree);


int BSTDetail(BSTNode* root);
int BSTInsert(BST* tree, ElemType data);
BSTNode* BSTLocate(BSTNode* root, ElemType data);
BSTNode* BSTLocateMax(BSTNode* root);
BSTNode* BSTLocateMin(BSTNode* root);
ElemType BSTMax(BSTNode* root);
ElemType BSTMin(BSTNode* root);
BSTNode* BSTParentOf(BSTNode* root, ElemType data);
BSTNode** BSTPointerOf(BST* tree, ElemType data);
int BSTPrint(BSTNode* root);
int BSTRemove(BST* tree, ElemType data);


struct bst {
	BSTNode*   root;
};


struct bstNode {
	ElemType   data;
	BSTNode*   left;
	BSTNode*   right;
};


#endif
