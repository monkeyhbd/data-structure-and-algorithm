#ifndef LIST_H
#define LIST_H


typedef int ElemType;
struct list;
struct listNode;
typedef struct list List;
typedef struct listNode ListNode;


int ListInit(List* list);
int ListDestroy(List* list);


// Clear all elements in list, get an empty list.
int ListClear(List* list);

// Print entire information of list, include the address of each node.
int ListDetail(List* list);

// Insert an element after a specific node.
int ListInsertAfter(ListNode* node, ElemType elem);

// Calculate the length of a list.
int ListLength(List* list);

// Locate the node which stored the specific element.
ListNode* ListLocate(List* list, ElemType elem);

// Pop a node in list.
int ListPop(List* list, ListNode* node);

// Print all elements in list.
int ListPrint(List* list);


#endif
